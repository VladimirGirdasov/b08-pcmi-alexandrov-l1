﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using P_Alexandrov__P1.Entities;
using P_Alexandrov__P1.Exceptions;

namespace P_Alexandrov__P1.Tests
{
	public abstract class GameTests
	{
		public abstract Type Type { get; }

		protected IGame CreateIGameInstance(Type type, IEnumerable args)
		{
			try
			{
				var game = (IGame) Activator.CreateInstance(type, args);
				return game;
			}
			catch (Exception e)
			{
				if (e.InnerException != null)
					throw e.InnerException;
				throw;
			}
		}
		
		/// <summary>
		/// Создаём доску, проверяем корректность расстановки фишек
		/// </summary>
		[TestMethod]
		public void BoardInitialization()
		{
			var game = CreateIGameInstance(Type, new int[] {1, 2, 3, 4, 5, 6, 0, 7, 8});

			Assert.AreEqual(2, game[0, 1], "Фишки расставались не правильно :(");
			Assert.AreEqual(6, game[1, 2], "Фишки расставались не правильно :(");
			Assert.AreEqual(0, game[2, 0], "Фишки расставались не правильно :(");
		}

		/// <summary>
		/// Пытаемся инициализировать доску фишками, количеством которых нельзя заполнить квадратную доску
		/// Должны словить соответствующее исключение
		/// </summary>
		[TestMethod]
		public void BoardInitializationWrongCountOfElements()
		{
			var exceptionCaught = false;
			
			try
			{
				var game = CreateIGameInstance(Type, new int[] { 1, 2 });
			}
			catch (WrongBoardElementsException e)
			{
				exceptionCaught = true;
			}

			Assert.IsTrue(exceptionCaught, "Я дал недостаточно фишек, где моё исключение?");
		}

		/// <summary>
		/// Пытаемся инициализировать доску фишками, где не обозначена пустая фишка, 
		/// Должны словить соответствующее исключение
		/// </summary>
		[TestMethod]
		public void BoardInitializationNoEmptyToken()
		{
			var exceptionCaught = false;

			try
			{
				var game = CreateIGameInstance(Type, new int[] { 1, 2, 3, 4 });
			}
			catch (WrongBoardElementsException e)
			{
				exceptionCaught = true;
			}

			Assert.IsTrue(exceptionCaught, "Я не обозначил пустую фишку, где моё исключение?");
		}

		/// <summary>
		/// Создаём доску с дубликатами, ожидаем исключение т.к. игра не предусматривает одинаковых фишек
		/// </summary>
		[TestMethod]
		public void TryingToCreateBoardWithDublicates()
		{
			var exceptionCaught = false;

			try
			{
				var game = CreateIGameInstance(Type, new int[] { 1, 2, 3, 3 });
			}
			catch (WrongBoardElementsException e)
			{
				exceptionCaught = true;
			}

			Assert.IsTrue(exceptionCaught, "В игре не может быть фишек дубликатов, где моё исключение?");
		}

		/// <summary>
		/// При обращение к несуществующей позиции доски, программа не должна упасть
		/// </summary>
		[TestMethod]
		public void AccessNonExistedPosition()
		{
			var game = CreateIGameInstance(Type, new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 0 });

			var val = game[int.MaxValue, int.MinValue];

			Assert.AreEqual(null, val);
		}

		/// <summary>
		/// Получаем позицию существующей фишки
		/// </summary>
		[TestMethod]
		public void GetLocationExistedValue()
		{
			var game = CreateIGameInstance(Type, new int[] { 1, 2, 3, 0 });

			var position = game.GetLocation(3);

			Assert.AreEqual(new Point(1, 0), position);
		}

		/// <summary>
		/// Получаем позицию не существующей фишки.
		/// Должны словить соответствующее исключение
		/// </summary>
		[TestMethod]
		public void GetLocationNotExistedValue()
		{
			var exceptionCaught = false;
			var game = CreateIGameInstance(Type, new int[] { 1, 2, 3, 0 });

			try
			{
				game.GetLocation(int.MaxValue);
			}
			catch (IndexOutOfRangeException e)
			{
				exceptionCaught = true;
			}

			Assert.IsTrue(exceptionCaught, "Запрошена не существующая фишка, где моё исключение?");
		}

		[TestMethod]
		public void ShiftCorrect()
		{
			var gameOld = CreateIGameInstance(Type, new int[] { 1, 2, 3, 4, 5, 6, 0, 7, 8 });

			var gameEdited = gameOld.Shift(7);

			Assert.AreEqual(new Point(2, 0), gameEdited.GetLocation(7), "Шифт не прошел");
			Assert.AreEqual(new Point(2, 1), gameEdited.GetLocation(0), "Шифт не прошел");

			if (Type.Name == typeof(GameImmutable).Name)
			{
				Assert.AreEqual(new Point(2, 1), gameOld.GetLocation(7), "Предшествующий объект изменился, а ведь он Immutable!");
				Assert.AreEqual(new Point(2, 0), gameOld.GetLocation(0), "Предшествующий объект изменился, а ведь он Immutable!");
			}
		}

		/// <summary>
		/// Двигаем фишку которую подвинуть нельзя.
		/// Должны словить соответствующее исключение
		/// </summary>
		[TestMethod]
		public void ShiftIncorrect()
		{
			var exceptionCaught = false;
			var game = CreateIGameInstance(Type, new int[] { 1, 2, 3, 4, 5, 6, 0, 7, 8 });

			try
			{
				game.Shift(1);
			}
			catch (GameException e)
			{
				exceptionCaught = true;
			}

			Assert.IsTrue(exceptionCaught, "При шифте значение не соседствующего с нулем не поймано исключение");
		}
	}
}
