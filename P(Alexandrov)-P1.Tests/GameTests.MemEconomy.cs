﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using P_Alexandrov__P1.Entities;

namespace P_Alexandrov__P1.Tests
{
	[TestClass]
	public class GameTestsMemEconomy : GameTests
	{
		public override Type Type => typeof(GameMemEconomy);

		/// <summary>
		/// Проверка на то, что декаратор действительно хранит историю предыдущих шагов и информацию об изменениях
		/// </summary>
		[TestMethod]
		public void GameSavesHistory()
		{
			var game = CreateIGameInstance(Type, new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 0 });

			game = game.Shift(8);
			game = game.Shift(5);

			Assert.AreEqual(0, game[1, 1]);
			Assert.AreEqual("Фишка со значением 5 изменила своё положение [1; 1] на положение [2; 1]", ((GameMemEconomy)game).StateInformation);

			Assert.AreEqual(0, ((GameMemEconomy)game).GamePreviousState[2, 1]);
			Assert.AreEqual("Фишка со значением 8 изменила своё положение [2; 1] на положение [2; 2]", ((GameMemEconomy)((GameMemEconomy)game).GamePreviousState).StateInformation);

			Assert.AreEqual(0, ((GameMemEconomy)((GameMemEconomy)game).GamePreviousState).GamePreviousState[2, 2]);
			Assert.AreEqual("Игра начата!", ((GameMemEconomy)((GameMemEconomy)((GameMemEconomy)game).GamePreviousState).GamePreviousState).StateInformation);
		}
	}
}
