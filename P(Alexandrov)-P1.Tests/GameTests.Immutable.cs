﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using P_Alexandrov__P1.Entities;

namespace P_Alexandrov__P1.Tests
{
	[TestClass]
	public class GameTestsImmutable : GameTests
	{
		public override Type Type => typeof(GameImmutable);
	}
}
