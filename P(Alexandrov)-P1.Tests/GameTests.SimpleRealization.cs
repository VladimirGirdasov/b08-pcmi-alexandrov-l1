﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using P_Alexandrov__P1.Entities;
using P_Alexandrov__P1.Exceptions;

namespace P_Alexandrov__P1.Tests
{
	[TestClass]
	public class GameTestsSimpleRealization : GameTests
	{
		public override Type Type => typeof(Game);
	}
}
