﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P_Alexandrov__P1.Exceptions
{
	public class GameException : Exception
	{
		public GameException(string message) : base(message)
		{

		}
	}
}
