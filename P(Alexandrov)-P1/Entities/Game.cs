﻿using System;
using System.Collections.Generic;
using System.Linq;
using P_Alexandrov__P1.Entities;
using P_Alexandrov__P1.Entities.Interfaces;
using P_Alexandrov__P1.Exceptions;
using P_Alexandrov__P1.Resources;

namespace P_Alexandrov__P1
{
	public class Game : IGame
	{
		public int BoardLength { get; protected set; }
		protected int[,] Board { get; set; }

		/// <summary>
		///     Хранит текующее положение фишек. Ключ - значение фишки, Значение - позиция
		/// </summary>
		protected Dictionary<int, Point> Locations { get; set; }

		/// <summary>
		///     Создаёт доску для игру в 15, с указанными значениями фишек
		/// </summary>
		/// <param name="boardElements">
		///     Желаемые значения фишек. Фишки должны заполнить собой
		///     квадратную доску. Среди фишек должна быть пустая фишка со значением 0.
		///     Среди фишек не должно быть дубликатов
		/// </param>
		public Game(params int[] boardElements)
		{
			if (Math.Sqrt(boardElements.Length)%1 != 0.0 || boardElements.Length == 1 || boardElements.Length == 0)
			{
				throw new WrongBoardElementsException(ErrorMesages.WrongCountOfBoardElements);
			}

			if (!boardElements.Contains(0))
			{
				throw new WrongBoardElementsException(ErrorMesages.NoEmptyTokensInBoardElements);
			}

			if (boardElements.Any(check => boardElements.Where(x => x == check).Count() > 1))
			{
				throw new WrongBoardElementsException(ErrorMesages.DublicatesOnBoard);
			}

			BoardLength = (int) Math.Sqrt(boardElements.Length);

			Board = new int[BoardLength, BoardLength];

			var car = 0;
			Locations = new Dictionary<int, Point>();
			foreach (var boardElement in boardElements)
			{
				var x = car/BoardLength;
				var y = car%BoardLength;

				Board[x, y] = boardElement;
				Locations[boardElement] = new Point(x, y);
				car++;
			}
		}

		/// <summary>
		/// Находит соседа с соответсвующим значением (Игнорируя соседа по диагонали).
		/// </summary>
		/// <param name="pointSource"> Исходная точка для которой ищим соседа </param>
		/// <param name="value"> Значение которое ищем </param>
		/// <returns> Возвращает <see cref="Point"/> на соседнюю позицию запрошенного значения или null </returns>
		protected Point FindNeighbour(Point pointSource, int value)
		{
			if (this[pointSource.X, pointSource.Y] == null || this[pointSource.X, pointSource.Y] == value || !Locations.ContainsKey(value))
				return null;

			if (this[pointSource.X - 1, pointSource.Y] == value)
				return new Point(pointSource.X - 1, pointSource.Y);

			if (this[pointSource.X + 1, pointSource.Y] == value)
				return new Point(pointSource.X + 1, pointSource.Y);

			if (this[pointSource.X, pointSource.Y - 1] == value)
				return new Point(pointSource.X, pointSource.Y - 1);

			if (this[pointSource.X, pointSource.Y + 1] == value)
				return new Point(pointSource.X, pointSource.Y + 1);

			return null;
		}

		public IPoint GetLocation(int value)
		{
			if (!Locations.ContainsKey(value))
				throw new IndexOutOfRangeException(ErrorMesages.NoTokenWithSuchValue);

			return Locations[value];
		}

		public virtual IGame Shift(int value)
		{
			var pointSource = GetLocation(value);

			var pointZero = FindNeighbour((Point)pointSource, 0);

			if (pointZero == null)
				throw new GameException(ErrorMesages.ShiftMethodCantFindZeroPointAsNeighbour);

			Board[pointZero.X, pointZero.Y] = value;
			Board[pointSource.X, pointSource.Y] = 0;

			Locations[value] = pointZero;
			Locations[0] = (Point)pointSource;

			return this;
		}

		/// <summary>
		///     Получение значения фишки в указанной позиции
		/// </summary>
		/// <param name="row">Номер ряда начиная с нуля</param>
		/// <param name="column">Номер колонки начиная с нуля</param>
		/// <returns>Значение фишки в указаной позиции</returns>
		public int? this[int row, int column]
		{
			get
			{
				if ((row > BoardLength - 1) || (column > BoardLength - 1) || row < 0 || column < 0)
				{
					return null;
				}
				return Board[row, column];
			}
		}
	}
}