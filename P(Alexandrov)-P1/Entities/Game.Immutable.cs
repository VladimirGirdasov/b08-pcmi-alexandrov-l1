﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using P_Alexandrov__P1.Exceptions;
using P_Alexandrov__P1.Extensions;
using P_Alexandrov__P1.Resources;

namespace P_Alexandrov__P1.Entities
{
	public class GameImmutable : Game
	{
		public GameImmutable(params int[] boardElements) : base(boardElements)
		{
			
		}

		public override IGame Shift(int value)
		{
			var pointSource = GetLocation(value);

			var pointZero = FindNeighbour((Point)pointSource, 0);

			if (pointZero == null)
				throw new GameException(ErrorMesages.ShiftMethodCantFindZeroPointAsNeighbour);

			var newBoard = Board.Clone() as int[,];
			if (newBoard == null)
			{
				throw new WrongBoardElementsException(ErrorMesages.CantImmutablyCreateNewBoardBecausePreviousIsCorrupted);
			}

			newBoard[pointZero.X, pointZero.Y] = value;
			newBoard[pointSource.X, pointSource.Y] = 0;

			return new GameImmutable(newBoard.To1DArray());
		}
	}
}
