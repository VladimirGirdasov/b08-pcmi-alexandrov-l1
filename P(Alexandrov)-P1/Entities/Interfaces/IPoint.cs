﻿namespace P_Alexandrov__P1.Entities.Interfaces
{
	public interface IPoint
	{
		int X { get; set; }
		int Y { get; set; }
	}
}