﻿using P_Alexandrov__P1.Entities;
using P_Alexandrov__P1.Entities.Interfaces;

namespace P_Alexandrov__P1
{
	public interface IGame
	{
		/// <summary>
		/// Возвращает расположение точки с указанным значением
		/// </summary>
		/// <param name="value"> Значение фишки, расположение которой мы ищем </param>
		/// <returns>Возвращается координаты <see cref="IPoint"/> расположения указанного на вход значения</returns>
		IPoint GetLocation(int value);

		/// <summary>
		/// Перемещает фишку с указанным значением на соседнее ближайшее поле
		/// </summary>
		/// <param name="value"> Значение фишки которую перемещаем </param>
		/// <returns> Возвращает объект игры <see cref="IGame"/> с передвинутой фишкой </returns>
		IGame Shift(int value);

		/// <summary>
		///     Получение значения фишки в указанной позиции
		/// </summary>
		/// <param name="row">Номер ряда начиная с нуля</param>
		/// <param name="column">Номер колонки начиная с нуля</param>
		/// <returns>Значение фишки в указаной позиции</returns>
		int? this[int row, int column] { get; }
	}
}