﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using P_Alexandrov__P1.Entities.Interfaces;

namespace P_Alexandrov__P1.Entities
{
	public class Point : IPoint
	{
		public int X { get; set; }

		public int Y { get; set; }

		public Point(int x, int y)
		{
			X = x;
			Y = y;
		}

		public override string ToString()
		{
			return $"X={this.X}, Y={this.Y}";
		}

		public override bool Equals(object obj)
		{
			var point = obj as Point;

			if (point?.X == X && point.Y == this.Y)
				return true;

			return false;
		}
	}
}
