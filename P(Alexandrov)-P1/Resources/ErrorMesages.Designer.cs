﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace P_Alexandrov__P1.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class ErrorMesages {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal ErrorMesages() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("P_Alexandrov__P1.Resources.ErrorMesages", typeof(ErrorMesages).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Невозможно создать новую Immutable доску, так текущая повреждена.
        /// </summary>
        internal static string CantImmutablyCreateNewBoardBecausePreviousIsCorrupted {
            get {
                return ResourceManager.GetString("CantImmutablyCreateNewBoardBecausePreviousIsCorrupted", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Игра 15 не предусматривает возможность дубликатов фишек.
        /// </summary>
        internal static string DublicatesOnBoard {
            get {
                return ResourceManager.GetString("DublicatesOnBoard", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Для игры необходимо обеспечить на доске пустое поле. Отметье его значением 0 в массиве.
        /// </summary>
        internal static string NoEmptyTokensInBoardElements {
            get {
                return ResourceManager.GetString("NoEmptyTokensInBoardElements", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Фишка с таким значением отсутствует.
        /// </summary>
        internal static string NoTokenWithSuchValue {
            get {
                return ResourceManager.GetString("NoTokenWithSuchValue", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Game.Shift не нашел нулевую фишку в соседстве с заданной фишкой.
        /// </summary>
        internal static string ShiftMethodCantFindZeroPointAsNeighbour {
            get {
                return ResourceManager.GetString("ShiftMethodCantFindZeroPointAsNeighbour", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Количество предоставленных фишек не позволяет заполнить ими квадратную доску.
        /// </summary>
        internal static string WrongCountOfBoardElements {
            get {
                return ResourceManager.GetString("WrongCountOfBoardElements", resourceCulture);
            }
        }
    }
}
