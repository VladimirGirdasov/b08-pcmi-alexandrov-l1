﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P_Alexandrov__P1.Extensions
{
	internal static class Int2DArrayExtensions
	{
		public static int[] To1DArray(this int[,] array)
		{
			var xLength = array.GetLength(1);
			var yLength = array.GetLength(0);

			var ans = new int[xLength * yLength];

			for (var x = 0; x < xLength; x++)
			{
				for (var y = 0; y < yLength; y++)
				{
					ans[x*xLength + y] = array[x, y];
				}
			}

			return ans;
		}
	}
}
